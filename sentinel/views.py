from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from sentinel.forms import SignUpForm, LoginForm
from rest_framework.permissions import IsAuthenticated
from sentinel.serializers import UserAuthTokenSerializer
from rest_framework import generics


def signupView(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            token, created = Token.objects.get_or_create(user=user)
            Response({'token': token.key, 'user_name': user.username})
            render(Response, 'welcome.html')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def loginView(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


class UserAuthTokenView(generics.ListAPIView):

    def get_queryset(self):
        user = self.request.user
        return Token.objects.filter(user=user)

    serializer_class = UserAuthTokenSerializer
