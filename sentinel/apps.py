from django.apps import AppConfig


class SentinelConfig(AppConfig):
    name = 'sentinel'

    def ready(self):
        import sentinel.signals
