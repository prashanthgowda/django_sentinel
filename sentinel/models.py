from django.db import models


class ClientUser(models.Model):
    client_user = models.CharField(max_length=500, editable=False)
    ip_address = models.CharField(max_length=39, editable=False)

    class Meta:
        unique_together = ("client_user", "ip_address")


class Error(models.Model):
    user = models.ForeignKey(ClientUser, on_delete=models.CASCADE)
    file_name = models.CharField(max_length=500, editable=False)
    module_name = models.CharField(max_length=500, editable=False)
    exception = models.CharField(max_length=500, editable=False)
    date = models.DateTimeField(auto_now_add=True, blank=True, editable=False)


class ErrorBucket(models.Model):
    error = models.ForeignKey(Error, on_delete=models.CASCADE)
    error_count = models.IntegerField(default=1)
    user_count = models.IntegerField(default=1)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
