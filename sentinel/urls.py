from django.urls import include, path
from django.conf.urls import url
from sentinel import views

urlpatterns = [
    url(r'^api-token-auth/', views.UserAuthTokenView.as_view(), name='token'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest')),
    url(r'^signup/$', views.signupView, name='account_signup'),
    url(r'^login/$', views.loginView, name='login_page'),
    # url(r'^home/$', views.homeView, name='home_page'),
]
